# Git 
cd ..
git clone https://bitbucket.org/ev45ive/jbi-angular-open-intro.git
cd jbi-angular-open-intro
npm i 
npm run start

# Updates
git stash -u 
git pull 
<!--  -->
git pull -u origin/master

# CLI
npm i -g @angular/cli
ng 
ng version
## windows
ng.cmd version
Angular CLI: 10.1.7

# New project
cd ..
ng new angular-open-intro
? Would you like to add Angular routing? Yes
? Which stylesheet format would you

# Start project
ng s -o
npm run start -- -o
npm run start -- --port 1234

# Chrome Augury extensio
https://chrome.google.com/webstore/detail/augury/elgalmkoelokbchhkhacckoklkejnhcd

## VS COde
Emmet  // build in (install for InteliJ)

https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode
https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher


# Architecture
ng g 

# Core module
ng g m core -m app
ng g i core/model/playlist

# API Services
ng g s --flat false core/services/playlists
ng g s --flat false core/services/music-search
ng g s --flat false core/services/auth
ng g s --flat false core/services/user

# Shared module
ng g m shared -m playlists -m music-search
<!-- Expose widgets to all parent modules -->
ng g p shared/pipes/yesno --export 

# Playlists feature module
ng g m playlists -m app --routing
<!-- / Containers ( pages / views ) -->
ng g c playlists/containers/playlists

<!-- / Components ( widgets / lego blocks ) -->
ng g c playlists/components/playlists-list
ng g c playlists/components/playlists-list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form


# MusicSearch feature module
ng g m music-search -m app --routing
### <!-- / Containers ( pages / views ) -->
ng g c music-search/containers/music-search
ng g c music-search/containers/album-details

### <!-- / Components ( widgets / lego blocks ) -->
ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card

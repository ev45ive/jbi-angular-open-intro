import { NgForOf, NgForOfContext } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

NgForOf
NgForOfContext // avaliable local variables

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
  // inputs:['playlists:items']
})
export class PlaylistsListComponent implements OnInit {

  @Input('items')
  playlists: Playlist[] = []
  
  @Input()
  selected: Playlist | null = null

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(item: Playlist) {
    this.selected = item

    this.selectedChange.emit(item)
  }

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

type PlaylistDraft = Pick<Playlist, 'name' | 'public' | 'description'>;

@Component({
  selector: 'app-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss']
})
export class PlaylistFormComponent implements OnInit, OnChanges {

  @Input()
  playlist!: Playlist

  draft?: Playlist


  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes)
    // this.draft.name = this.playlist.name
    // this.draft = {  ...this.playlist  }
  }

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  constructor() { }

  cancelClicked() { this.cancel.emit() }

  formSubmitted(draft: PlaylistDraft) {
    
    const updatedPlaylist = {
      ...this.playlist,
      ...draft
    }
    this.save.emit(updatedPlaylist)
  }

  ngOnInit(): void {
  }

}

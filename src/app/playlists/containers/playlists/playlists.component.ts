import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

// enum ViewModesEnum { details, form }
type ViewModes = 'details' | 'form';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {
  mode: ViewModes = 'details'

  playlists: Playlist[] = [
    {
      id: '123',
      name: 'Angular Top20',
      public: true,
      description: 'I love pancakes'
    },
    {
      id: '234',
      name: 'Angular HIts',
      public: false,
      description: 'I love react :P'
    },
    {
      id: '345',
      name: 'Best of Angular ',
      public: true,
      description: 'I love Best of'
    },
  ]

  selected: Playlist | null = null

  switchPlaylist(item: Playlist | null) {
    // this.selected = item
    this.selected = this.selected?.id == item?.id ? null : item
  }

  constructor() { }

  ngOnInit(): void {
  }

  switchToForm() {
    this.mode = 'form'
  }

  switchToDetails() {
    this.mode = 'details'
  }

  savePlaylist(draft: any) {
    const index = this.playlists.findIndex(p => p.id === draft.id)
    if (index !== -1) {
      // this.playlists[index] = draft
      this.playlists.splice(index, 1, draft)
      this.selected = draft
      this.switchToDetails()
    }
    // console.log('Save clicked!', draft)
  }


}


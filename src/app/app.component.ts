import { ChangeDetectorRef, Component } from '@angular/core';

@Component({
  selector: 'app-root, app-like-pancakes',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = `JBI Training`;

  navOpen = false

  counter = 0

  constructor(/* cdr:ChangeDetectorRef */) {
    // console.log(setInterval) // zone.js wrapper
    // setInterval(() => {
    //   this.counter += 1

    //   // cdr.detectChanges()
    // }, 1000)
  }

  user = {
    name: 'Anonym',
    loggedin: false,
    greet(greeting: any) {
      return `${greeting} ${this.name}`
    }
  }

  login() {
    this.user.loggedin = true
    this.user.name = "Alice The User"
  }

  alert(msg: string) {
    // ExpressionChangedAfterItHasBeenCheckedError
    // this.title = msg 

    // window.alert(msg)
    // alert(msg)
    console.log(msg)
  }

}
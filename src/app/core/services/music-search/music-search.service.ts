import { Inject, Injectable } from '@angular/core';
import { SEARCH_API_URL } from 'src/app/music-search/tokens';
import { mockResults } from './mockResults';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { AuthService } from '../auth/auth.service';
import { Album, AlbumsSearchResponse } from '../../model/albums';

import { catchError, map, pluck } from 'rxjs/operators'
import { of, throwError } from 'rxjs';

@Injectable({
  // providedIn: MusicSearchModule,
  providedIn: 'root',
})
export class MusicSearchService {

  constructor(
    @Inject(SEARCH_API_URL) private api_url: string,
    private http: HttpClient,
    private auth: AuthService
  ) { }

  searchAlbums(query = 'batman') {
    // return this.http.get<AlbumsSearchResponse>(this.api_url).toPromise()

    return this.http.get<AlbumsSearchResponse>(this.api_url, {
      params: {
        q: query,
        type: 'album'
      },
      headers: {
        'Authorization': `Bearer ${this.auth.getToken()}`
      },
    }).pipe(
      // operators
      // pluck('albums','items'),
      map(result => (result.albums.items)),
      catchError(err => {
        if (err instanceof HttpErrorResponse) {
          return throwError(new Error(err.error.error.message))
        }
        return throwError(err)
        // return of(mockResults )
      })
    )


  }
}

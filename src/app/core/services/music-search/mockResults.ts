import { Album } from 'src/app/core/model/albums';

export const mockResults = [
  {
    id: '123', name: 'Test 123', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/500/500' }
    ]
  } as Album,
  {
    id: '234', name: 'Test 234', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/300/300' }
    ]
  } as Album,
  {
    id: '345', name: 'Test 345', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/400/400' }
    ]
  } as Album,
];

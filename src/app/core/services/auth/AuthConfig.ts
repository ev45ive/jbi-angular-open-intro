
export abstract class AuthConfig {
  auth_url!: string;
  /**
   The client ID provided to you by Spotify when you register your application.
   */
  client_id!: string;
  /**
   *
  Set it to “token”.
   */
  response_type!: string;
  redirect_uri!: string;
  state?: string;
  /**
   A space - separated list of scopes: see Using Scopes.
   */
  scopes?: string[];
  /**
   Whether or not to force the user to approve the app again if they’ve already done so.If false(default ), a user who has already approved the application may be automatically redirected to the URI specified by redirect_uri.If true, the user will not be automatically redirected and will have to approve the app again.
   */
  show_dialog?: boolean;
}

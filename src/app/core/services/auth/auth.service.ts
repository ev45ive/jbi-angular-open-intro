import { HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AuthConfig } from './AuthConfig';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: string | null = null

  constructor(
    @Inject(AuthConfig) private config: AuthConfig
  ) { }

  authorize() {
    sessionStorage.removeItem('token')

    const {
      auth_url, state, show_dialog, scopes,
      response_type, client_id,
      redirect_uri } = this.config

    const params = new HttpParams({
      fromObject: {
        client_id,
        response_type,
        redirect_uri,
        show_dialog: show_dialog ? 'true' : 'false',
        scope: scopes?.join(' ') || '',
        state: state || '',
      }
    })

    window.location.href = (`${auth_url}?${params.toString()}`)
  }

  init() {
    this.extractToken()

    if (!this.token) {
      this.authorize()
    }
  }


  extractToken() {
    let access_token: string | null = null;

    try {
      access_token = JSON.parse(sessionStorage.getItem('token')!)
    } catch (err) { }

    if (window.location.hash) {
      const params = new HttpParams({
        fromString: window.location.hash.slice(1)
      })
      access_token = params.get('access_token')
      if (access_token) {
        window.location.href = ''
      }
    }

    if (access_token) {
      this.token = access_token;
      sessionStorage.setItem('token', JSON.stringify(access_token))
    }
  }

  getToken() {
    return this.token
  }
}

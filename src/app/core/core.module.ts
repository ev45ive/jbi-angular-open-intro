import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthConfig } from './services/auth/AuthConfig';
import { AuthService } from './services/auth/auth.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: AuthConfig,
      useValue: environment.authConfig
    }
  ]
})
export class CoreModule { 
  constructor(private auth:AuthService){
    this.auth.init()
  }
}

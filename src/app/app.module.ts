import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { CoreModule } from './core/core.module';
import { MusicSearchModule } from './music-search/music-search.module';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    // == Libs
    // SuperHyperExtraCoolWidgetModule
    BrowserModule,
    // == Services
    CoreModule,
    // == Features
    MusicSearchModule,
    PlaylistsModule,
    // == App / Top level stuff
    AppRoutingModule,
  ],
  providers: [
    // {
    //   provide: 'SEARCH_API_URL',
    //   useValue: 'here override default from Search module'
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'music',
    pathMatch: 'full' // or 'prefix'
  },
  {
    path: '**', // catch all
    // component: PageNotFoundComponent
    redirectTo: 'music',
    pathMatch: 'full' // or 'prefix'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

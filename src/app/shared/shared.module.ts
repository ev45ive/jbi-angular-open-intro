import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YesnoPipe } from './pipes/yesno.pipe';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    YesnoPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    YesnoPipe,
    FormsModule,
  ] // share this with parent module
})
export class SharedModule { }

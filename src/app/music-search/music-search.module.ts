import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { MusicSearchComponent } from './containers/music-search/music-search.component';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { SharedModule } from '../shared/shared.module';
import { environment } from 'src/environments/environment';
import { SEARCH_API_URL } from './tokens';

@NgModule({
  declarations: [
    MusicSearchComponent,
    AlbumDetailsComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MusicSearchRoutingModule
  ],
  providers: [
    // {
    //   provide: HttpClient,
    //   useClass: MyMuchBetterHttpClient,
    // },
    {
      provide: SEARCH_API_URL,
      useValue: environment.search.api_url
    }
    // {
    //   provide: MusicSearchService,
    //   useFactory(url: string /* , routes:Route[][] */) {
    //     return new MusicSearchService(url)
    //   },
    //   // deps: [SEARCH_API_URL/* , ROUTES */]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: SpotifyMusicSearchService
    // },
    // MusicSearchService,
  ]
})
export class MusicSearchModule { }

import { InjectionToken } from '@angular/core';

const tokens = {};
export const SEARCH_API_URL = new InjectionToken<string>('API url for search');

import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Album } from 'src/app/core/model/albums';
import { mockResults } from 'src/app/core/services/music-search/mockResults';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';


@Component({
  selector: 'app-music-search',
  templateUrl: './music-search.component.html',
  styleUrls: ['./music-search.component.scss']
})
export class MusicSearchComponent implements OnInit {
  query = ''
  results: Album[] = mockResults
  message = ''

  constructor(
    // @Inject(MusicSearchService)
    private service: MusicSearchService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.query = this.route.snapshot.queryParamMap.get('q') || ''
    if (this.query) {
      this.searchAlbums(this.query)
    }
  }

  searchAlbums(query: string) {
    this.router.navigate(['/music'], {
      queryParams: {
        q: query
      }
    })

    this.service.searchAlbums(query).subscribe({
      next: result => this.results = (result),
      error: error => this.message = (error.message),
    })

  }



}


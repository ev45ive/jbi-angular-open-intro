import { AuthConfig } from 'src/app/core/services/auth/AuthConfig';

export const environment = {
  production: true,
  
  search: {
    api_url: 'https://api.spotify.com/v1/search'
  },
  authConfig: {
    auth_url: 'https://accounts.spotify.com/authorize',
    client_id: 'e0502baab8a94648b7cfe65e41fa2871',
    redirect_uri: 'http://localhost:4200/',
    response_type: 'token',
    scopes: [],
    show_dialog: true,
    state: ''
  } as AuthConfig
};

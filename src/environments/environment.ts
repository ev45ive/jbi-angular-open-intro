// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { AuthConfig } from "src/app/core/services/auth/AuthConfig";

export const environment = {
  production: false,
  search: {
    api_url: 'https://api.spotify.com/v1/search'
  },
  authConfig: {
    auth_url: 'https://accounts.spotify.com/authorize',
    client_id: 'e0502baab8a94648b7cfe65e41fa2871',
    redirect_uri: 'http://localhost:4200/',
    response_type: 'token',
    scopes: [],
    show_dialog: true,
    state: ''
  } as AuthConfig
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
